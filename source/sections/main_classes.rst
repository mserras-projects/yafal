.. _main:

============
Main Classes
============

This document summarizes the main classes that are used in YAFAL!

Data handlers
=============
The YAFAL dataset and Dataset corruptors are used to handle the data to fed the
YAFAL regressor.

.. _yafal dataset:

YAFAL Dataset
-------------

Creates a dataset with samples and their labels

.. autoclass:: yafal.YAFALDataset
   :members:

.. _dataset corruptor:

Dataset Corruptor
-----------------

Class that is employed to corrupt the Dataset

.. autoclass:: yafal.DatasetCorruptor
   :members:

Torch Dataset Handler
---------------------

Data handler to feed the YAFALDataset to Torch

.. autoclass:: yafal.YAFALTorchDataset
   :members:


Label Encoding
---------------

Encodes and/or describes the labels of a sample to train a Binary/Semantic YAFALRegressor

.. _label encoder:

.. autoclass:: yafal.YAFALLabelEncoder
   :members:



YAFAL Regression
================

Main Class that handles the training of YAFAL models

.. _regression:

.. autoclass:: yafal.YAFALRegression
   :members:

YAFAL Semantic
--------------
NN Model of the Semantic YAFAL regressor

.. autoclass:: yafal.SemanticYAFAL
   :members:

YAFAL Binary
-------------
NN Model of the Binary YAFAL regressor

.. autoclass:: yafal.BinaryYAFAL
   :members:
