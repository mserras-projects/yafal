
class InvalidMethodException(Exception):
    pass


class DescriptorsRequiredException(Exception):
    pass


class LabelInconsistencyException(Exception):
    pass


class LabelTypeException(Exception):
    pass
