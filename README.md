YAFAL
=====

You are fake labels! is a library which employs Large Language Models (LLMs)
to detect fake labels for text classification.

It uses Huggingface Transformers library with the PyTorch backend.

For more information, refer to the auto-generated [documentation](https://yafal.llinguai.eus)

Installation
============

To install it, just use:

    pip install yafal

Generating the Docs
-------------------
To auto-generate the documentation you will also need the following imports

    pip install sphinx numpydoc

Current Limitations
===================

Currently, these are the existing limitations, help is appreciated: 

 - Multi-label fake-label detection is only supported for the "binary" encoding method.
 - Dataset Corruption:
   - No data-conditioning exists, i.e. P(X, Y_corrupt) = P(X)P(Y_corrupt)
   - There is no method to take into account a label-corruption confusion matrix: P(y_c | y))
 - Export to ONNX yet to be done
 - Sphinx Documentation
 - YAFALDocumentHandler
 - Test it on GPU settings

 License - MIT
=======

Copyright 2022, M. Serras

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
